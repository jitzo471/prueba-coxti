import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { HomePageComponent } from './home-page/home-page.component';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { PrivacityPoliceComponent } from './privacity-police/privacity-police.component';

const routes: Routes = [
    { path: '', component: HomePageComponent },
    { path: 'iniciar-sesion', component: LoginComponent },
    { path: 'registrarse', component: SignupComponent },
    { path: 'politica-privacidad', component: PrivacityPoliceComponent },
    //{ path: 'info-usuario', component: ServerErrorComponent },
    { path: '**', component: NotFoundComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
