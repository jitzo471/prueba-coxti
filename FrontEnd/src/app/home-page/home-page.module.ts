import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { HomePageComponent } from './home-page.component';
import { HomePageRoutingModule } from './home-page-routing.module';
import { NavbarUserComponent } from '../navbar-user/navbar-user.component';
import { NavbarUserModule } from '../navbar-user/navbas-user.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        HomePageRoutingModule
    ],
    declarations: []
})

export class HomePageModule {

}
