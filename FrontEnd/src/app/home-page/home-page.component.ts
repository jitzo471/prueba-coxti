import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment.prod';
import { Router } from '@angular/router';
import { GlobalApp } from '../shared/services/globalApp';
import { CrudServices } from '../shared/services/crud-service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss']
})
export class HomePageComponent implements OnInit {

    public actualDate = new Date().getFullYear();
    public user: any = {};

    constructor(
        private router: Router,
        public app: GlobalApp,
        private crudService: CrudServices
    ) { }

    ngOnInit() {
        if (this.app.getItem('api_key')) {
            this.crudService.getRequest('api/user/get/' + this.app.getItem('user_id')).subscribe(
                data => {
                    const json: any = data;
                    this.user = json.user;
                },
                error => {
                    console.error();
                }
            );
        }
    }

    getUser() {

    }

    /**
     * Función para cerrar la sesión del usuario
     */
    onLoggedout() {
        window.sessionStorage.clear();
        this.router.navigate(['/']);
    }

}
