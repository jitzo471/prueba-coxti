import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';
import { NavbarUserRoutingModule } from './navbar-user-routing.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        NavbarUserRoutingModule
    ],
    declarations: []
})
export class NavbarUserModule {}
