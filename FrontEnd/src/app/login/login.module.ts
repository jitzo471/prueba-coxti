import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

import { LoginRoutingModule } from './login-routing.module';
import { LoginComponent } from './login.component';
import { NavbarUserComponent } from '../navbar-user/navbar-user.component';
import { NavbarUserModule } from '../navbar-user/navbas-user.module';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,

        LoginRoutingModule
    ],

    declarations: []

})
export class LoginModule {}
