import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { LoginCredentials } from '../shared/models/login-credentials';
import { CrudServices } from '../shared/services/crud-service';
import swal from 'sweetalert2';
import { environment } from 'src/environments/environment.prod';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {

    public loginCredentials = new LoginCredentials();

    constructor(
        private router: Router,
        private crudService: CrudServices
    ) {}

    ngOnInit() {

    }

    /**
     * Función que realiza la petición para el proceso de autenticación del usuario
     */
    onSignIn() {
        this.crudService.authenticate(this.loginCredentials).subscribe(
            data => {
                const response: any = data;
                sessionStorage.setItem('api_key', response.api_key);
                sessionStorage.setItem('user_name', response.user.full_name);
                sessionStorage.setItem('user_id', response.user.id);
                swal.fire('Hola '+response.user.full_name+'!', 'Te damos la bienvenida a COXTI.', 'success');
                this.router.navigateByUrl('/');
            },
            error => {
                swal.fire('ERROR!', error.error.message, 'error');
                console.error(error);
            }
        );
    }

    onSingUp() {
        this.router.navigate(['/registrarse']);

    }
}
