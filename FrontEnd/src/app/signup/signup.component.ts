import { Component, OnInit, ViewChild } from '@angular/core';
import { routerTransition } from '../router.animations';
import { CrudServices } from '../shared/services/crud-service';
import swal from 'sweetalert2';
import { LoginCredentials } from '../shared/models/login-credentials';
import { Router } from '@angular/router';
import { JsonService } from '../shared/services/json-service';
import { User } from '../shared/models/user';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss'],
    animations: [routerTransition()]
})
export class SignupComponent implements OnInit {
    public user = new User;
    public passwordError = true;
    public deparments: any = [];
    public cities: any = [];
    public deparment = '';
    public city = '';
    public emailFlag = false;
    public disableLocation= true;
    public disableFinancial= true;

    constructor(private crudService: CrudServices, private jsonService: JsonService, private router: Router) {
    }

    ngOnInit() {
        $('#email').on('keyup change', function (e) {
            const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (emailRegex.test($('#email').val() + '')) {
                this.style.backgroundColor = '';
            } else {
                this.style.backgroundColor = 'LightPink';
            }
        });
        this.getDeparments();
    }

    // Obtiene lista de departamentos
    getDeparments() {
        this.deparments = this.jsonService.getDeparments();
    }

    // Obtiene lista de las ciudades del departamento seleccionado
    getCities() {
        this.cities = this.jsonService.getCities(this.user.deparment);
    }

    // Realiza el registro del usuario en el sistema
    onSubmit() {
        this.passwordConfirm();

        if (!this.passwordError) {
            console.log('Enviando: ', this.user);

            this.crudService.postRequest(this.user, 'api/user/store').subscribe(
                data => {
                    swal.fire('EXCELENTE!', 'Te has registrado correctamente.', 'success');
                    this.router.navigateByUrl('dashboard');
                },
                error => {
                    let errorMessage = '';
                    if (error.error.document !== undefined) {
                        errorMessage = errorMessage + '<p>Cédula ya ha sido registrada.</p>';
                    }
                    if (error.error.email !== undefined) {
                        errorMessage = errorMessage + '<p>Correo ya ha sido registrado.<p>';
                    }
                    swal.fire({
                        title: 'ERROR EN EL REGISTRO!',
                        html: errorMessage,
                        type: 'error'
                    });
                    console.error(error);
                }
            );
        }

    }

    // Confirma si los dos campos de contraseña coinciden
    passwordConfirm() {
        if (this.user.password.toString().length === this.user.confirmPassword.toString().length) {
            if (this.user.password !== this.user.confirmPassword) {
                this.passwordError = true;
            } else {
                this.passwordError = false;
            }
        } else {
            this.passwordError = true;
        }
    }

    // Validación para los input de tipo number
    validateNumberInput(input, required: boolean) {
        if (required) {
            if (input === null || !input || parseInt(input) <= 0) {
                return true;
            }
            return false;
        } else {
            if (input === null || parseInt(input) <= 0) {
                return true;
            }
            return false;
        }
    }

    // Valida si un input está vacío
    validateEmptyInput(input) {
        if (input === null || !input) {
            return true;
        }
        return false;
    }

    validateEmailInput(email) {
        const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (email && !emailRegex.test(email)) {
            return true;
        }
        return false;
    }

    changeTab(t, tab) {
        switch (tab) {
            case 'locationInfo':
              this.disableLocation= false;
              break;

            case 'financialInfo':
              this.disableFinancial = false;
              break;
          }
          setTimeout(() => {
            t.select(tab);
          }, 100);
    }
}
