import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment.prod';

@Injectable({ providedIn: 'root' })

export class JsonService {

    serverUrl = environment.serverUrl;
    deparments: any = [];
    cities: any = [];

    constructor(private http: HttpClient) {
    }

    public getJSON(): Observable<any> {
        return this.http.get('./assets/colombia.json');
    }

    public getDeparments(): any {
        this.http.get('./assets/colombia.json').subscribe(
            data => {
                const json = JSON.parse(JSON.stringify(data));
                json.forEach(deparment => {
                    this.deparments.push({name: deparment.departamento});
                });
            }
        );
        return this.deparments;
    }

    public getCities(deparmentToSearch) {
        this.cities = [];
        if (deparmentToSearch === '') {
            this.cities = [];
        } else {
            this.http.get('./assets/colombia.json').subscribe(
                data => {
                    const json = JSON.parse(JSON.stringify(data));
                    json.forEach(deparment => {
                        if (deparment.departamento === deparmentToSearch) {
                            deparment.ciudades.forEach(city => {
                                this.cities.push({name: city});
                            });
                        }
                    });
                }
            );
        }
        return this.cities;
    }


}
