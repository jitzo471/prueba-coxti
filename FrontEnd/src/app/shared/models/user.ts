export class User {
    fullName: string;
    document: number;
    cellPhone: number;
    email: string;
    password: string;
    confirmPassword: string;
    deparment: string;
    city: string;
    district: string;
    address: string;
    salary: string;
    otherFounds;
    monthlyExpenses: string;
    financialExpenses: string;

    constructor() {
      this.fullName = '';
      this.document = null;
      this.cellPhone = null;
      this.email = '';
      this.password = '';
      this.confirmPassword = '';
      this.deparment = '';
      this.city = '';
      this.district = '';
      this.address = '';
      this.salary = '';
      this.otherFounds = '';
      this.monthlyExpenses = '';
      this.financialExpenses = null;
    }
}
