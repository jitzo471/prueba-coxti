<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Str;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Función que realiza la autenticación del usuario
     */
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::where('email', $request->input('email'))->first();
        if ($user && Hash::check($request->input('password'), $user->password)) {
            $apikey = base64_encode(Str::random(40));
            User::where('email', $request->input('email'))->update(['api_key' => $apikey]);
            $user = User::where('email', $request->input('email'))->first();
            return response()->json([
                'status' => 'OK',
                'success' => true,
                'api_key' => $apikey,
                'user' => $user
            ], 200);
        } else {
            return response()->json([
                'status' => 'ERROR',
                'success' => false,
                'message' => 'Credenciales incorrectas'
            ], 401);
        }
    }
}
