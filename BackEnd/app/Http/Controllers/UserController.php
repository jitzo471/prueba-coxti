<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Función para obtener todos los usuarios registrados en el sistema
     */
    public function getAll()
    {
        $this->middleware('auth');
        $users = User::all();
        return response()->json([
            'status' => 'OK',
            'success' => true,
            'users' => $users
        ]);
    }

    /**
     * Función para obtener un usuario por su ID
     */
    public function get($id)
    {
        $this->middleware('auth');
        $user = User::find($id);
        if ($user) {
            return response()->json([
                'status' => 'OK',
                'success' => true,
                'user' => $user
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'success' => false,
            'message' => 'El usuario solicitado no existe'
        ]);
    }

    /**
     * Función para obtener un usuario por su ID
     */
    public function store(Request $request) {
        $validator = Validator::make(
            $request->json()->all(),
            [
                'email' => 'unique:users',
                'document' => 'unique:users'
            ],
            [
                'email.unique' => 'El Correo electrónico ingresado ya ha sido registrado',
                'document.unique' => 'El Número de identificación ingresado ya ha sido registrado'
            ]
        );

        if ($validator->fails()) {
            return response()->json($validator->errors(), 403);
        }

        $user = User::create([
            'full_name' => $request->input('fullName'),
            'document' => $request->input('document'),
            'cell_phone' => $request->input('cellPhone'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'deparment' => $request->input('deparment'),
            'city' => $request->input('city'),
            'district' => $request->input('district'),
            'address' => $request->input('address'),
            'salary' => $request->input('salary'),
            'other_founds' => $request->input('otherFounds'),
            'monthly_expenses' => $request->input('monthlyExpenses'),
            'financial_expenses' => $request->input('financialExpenses'),
        ]);

        return response()->json([
            'status' => 'OK',
            'success' => true,
            'message' => 'Información registrada correctamente',
            'user' => $user
        ]);
    }

    /**
     * Función para actualizar la información de un usuario
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        if ($request->input('email') != $user->email) {
            $this->validate($request, [
                'email' => 'unique:users'
            ]);
        }

        $user->update([
            'name' => $request->input('name'),
            'email' => $request->input('email')
        ]);

        if ($user) {
            return response()->json([
                'status' => 'OK',
                'success' => true,
                'message' => 'Información actualizada correctamente'
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'success' => false,
            'message' => 'La información del usuario no pudo ser actualizada'
        ]);
    }

    /**
     * Función para actualizar la información de un usuario
     */
    public function changePassword(Request $request, $id)
    {
        $user = User::findOrFail($id);
        $user->update([
            'password' => Hash::make($request->input('password'))
        ]);

        if ($user) {
            return response()->json([
                'status' => 'OK',
                'success' => true,
                'message' => 'Contraseña actualizada correctamente'
            ]);
        }

        return response()->json([
            'status' => 'ERROR',
            'success' => false,
            'message' => 'La contraseña no pudo ser actualizada'
        ]);
    }
}
