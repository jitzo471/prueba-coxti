<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Auth\Authenticatable as AuthenticableTrait;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Model implements Authenticatable
{
    //
    use SoftDeletes;
    use AuthenticableTrait;
    protected $fillable = [
        'full_name', 'document', 'cell_phone', 'email',
        'password', 'deparment', 'city', 'district',
        'address', 'salary', 'other_founds', 'monthly_expenses',
        'financial_expenses'
    ];
    protected $hidden = [
        'password'
    ];

}
