<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/php-info', function () use ($router) {
    echo phpinfo();
});

$router->group(['prefix' => 'api/'], function ($router) {
    $router->post('authenticate', 'LoginController@authenticate');
});

$router->group(['prefix' => 'api/user/'], function ($router) {
    $router->get('getAll', 'UserController@getAll');
    $router->get('get/{id}', 'UserController@get');
    $router->post('store', 'UserController@store');
});
