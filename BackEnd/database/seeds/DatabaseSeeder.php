<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        //Disable foreign key check for this connection before running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');

        //Call entity seeders

        //Enable foreign key check for this connection after running seeders
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

    }
}
