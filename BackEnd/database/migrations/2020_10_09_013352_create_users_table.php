<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('full_name');
            $table->string('document')->unique();
            $table->string('cell_phone');
            $table->string('email')->unique();
            $table->string('password');
            $table->string('deparment');
            $table->string('city');
            $table->string('district');
            $table->string('address');
            $table->string('salary');
            $table->string('other_founds')->nullable();
            $table->string('monthly_expenses')->nullable();
            $table->string('financial_expenses')->nullable();


            $table->string('api_key')->nullable();

            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
